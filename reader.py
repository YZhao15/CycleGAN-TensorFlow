import tensorflow as tf
import utils


class Reader():
    def __init__(self, tfrecords_file, image_size=256,
                 buffer_size=1000, batch_size=1, num_threads=8, name=''):
        """
    Args:
      tfrecords_file: string, tfrecords file path
      buffer_size: integer, minimum number of samples to retain in the queue that provides of batches of examples
      batch_size: integer, number of images per batch
      num_threads: integer, number of preprocess threads
    """
        self.tfrecords_file = tfrecords_file
        self.image_size = image_size
        self.buffer_size = buffer_size
        self.batch_size = batch_size
        self.num_threads = num_threads
        self.raw_image_dataset = tf.data.TFRecordDataset(tfrecords_file)
        self.name = name

    def feed(self):
        """
    Returns:
      images: 4D tensor [batch_size, image_width, image_height, image_depth]
    """
        with tf.name_scope(self.name):
            parsed_image_dataset = self.raw_image_dataset \
                .map(self._parse_image_function, self.num_threads) \
                .map(self._preprocess, self.num_threads) \
                .shuffle(self.buffer_size) \
                .batch(self.batch_size, drop_remainder=True) \
                .prefetch(self.batch_size)

            iterator = tf.compat.v1.data.make_one_shot_iterator(parsed_image_dataset)
            next_images = iterator.get_next()

            tf.compat.v1.summary.image('input', next_images)
        return next_images

    def _preprocess(self, tfrecord_item):
        image_encoded_jpeg = tfrecord_item['image/encoded_image']
        image = tf.io.decode_jpeg(image_encoded_jpeg, channels=3)
        image = tf.image.resize(image, size=(self.image_size, self.image_size))
        image = utils.convert2float(image)

        return image

    def _parse_image_function(self, example_proto):
        return tf.io.parse_single_example(
            example_proto,
            features={
                'image/file_name': tf.io.FixedLenFeature([], tf.string),
                'image/encoded_image': tf.io.FixedLenFeature([], tf.string),
            })


def test_reader():
    TRAIN_FILE_1 = 'data/tfrecords/young.tfrecords'
    TRAIN_FILE_2 = 'data/tfrecords/old.tfrecords'

    graph = tf.Graph()
    with graph.as_default():
        reader1 = Reader(TRAIN_FILE_1, image_size=512, batch_size=1, name='young')
        reader2 = Reader(TRAIN_FILE_2, image_size=512, batch_size=1, name='old')
        images_op1 = reader1.feed()
        images_op2 = reader2.feed()

        summary_op = tf.compat.v1.summary.merge_all()
        test_writer = tf.compat.v1.summary.FileWriter('logs', graph)

        print(images_op1.shape)
        print(images_op1.shape[0])
        print(images_op1.shape.as_list())

        sess = tf.compat.v1.Session()
        init = tf.compat.v1.global_variables_initializer()
        sess.run(init)

        coord = tf.train.Coordinator()

        try:
            step = 0
            while not coord.should_stop():
                batch_images1, batch_images2, summary = sess.run([images_op1, images_op2, summary_op])
                # print("image shape: {}".format(batch_images2.shape))
                # print("image shape: {}".format(batch_images1.shape))

                test_writer.add_summary(summary, step)
                test_writer.flush()

                print("=" * 10)
                step += 1
        except KeyboardInterrupt:
            print('Interrupted')
            coord.request_stop()
        except Exception as e:
            coord.request_stop(e)
        finally:
            # When done, ask the threads to stop.
            coord.request_stop()
            sess.close()


if __name__ == '__main__':
    test_reader()
